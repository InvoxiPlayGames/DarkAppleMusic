# Dark Apple Music
Dark Apple Music is a dark CSS theme for Apple Music on iTunes 12.2+ (only tested on iTunes 12.6.3 on Windows 10)

## How to apply
1. Append the contents of DarkAppleMusic.css to iTunes.css - On Windows, iTunes.css is located at (iTunes install dir)/iTunes.resources/iTunes.css (don't use Notepad)
2. Restart iTunes
3. Enjoy!

## How to find CSS classes
As there is no easy way to find CSS classes inside iTunes, you might have to try finding elements manually in the Apple Music desktop CSS files. [You can find the CSS file for Apple Music here.](https://desktop-music.itunes.apple.com/assets/desktop-music-app-25ddada8c765727add2ba6001f74cc90.css).